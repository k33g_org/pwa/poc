# pwa poc

## Initialize

- Build with the help of https://simplepwa.com/
- Replace in the `public` directory the `favicon.png` by your `favicon.png` (eg: in `assets` directory)
- Update `favicon_config.json` if needed (eg: `backgroundColor`, `themeColor`)
- Run `real-favicon generate favicon_config.json favicon_data.json .` (in `public` directory)

- The resulting `favicon_data.json` and `site.webmanifest` can be discarded.

## Serve

```bash
node index.js
```

Or you can deploy it as a GitLab Page.

🖐 Before any deployment, change the value of `cacheName` in `sw.js`